from django import forms
from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.contrib.auth import admin as auth_admin
from django.contrib.auth.models import Group

from apps.accounts.models import User
from apps.api.models import NoteToken


class CustomUserChangeForm(forms.ModelForm):
    """
    The form to update a :class:`accounts.models.User` in the
    administration interface.
    """

    def clean_password(self):
        """
        A stub function to prevent errors when validating the form.
        """
        pass


class TokenAdmin(admin.StackedInline):
    max_num = 1
    min_num = 0
    model = NoteToken
    can_delete = False
    show_change_link = True
    readonly_fields = ["key"]


class TokenFilter(SimpleListFilter):
    title = "has_token"
    parameter_name = "has_token"

    def lookups(self, request, model_admin):
        return [("✓", "True"), ("x", "False")]

    def queryset(self, request, queryset):
        if self.value() == "✓":
            return queryset.filter(auth_token__isnull=False)
        if self.value():
            return queryset.filter(auth_token__isnull=True)


class UserAdmin(auth_admin.UserAdmin):
    form = CustomUserChangeForm

    list_display = (
        "email",
        "lastname",
        "firstname",
        "id",
        "is_superuser",
        "date_joined",
        "last_login",
        "auth_token",
    )
    list_filter = ("is_superuser", "groups", TokenFilter)
    search_fields = ("email",)
    ordering = ("email",)
    filter_horizontal = ("groups",)

    fieldsets = (
        (None, {"fields": ("email", "lastname", "firstname")}),
        ("Permissions", {"fields": ("is_superuser", "groups")}),
    )
    readonly_fields = ("email",)

    inlines = [TokenAdmin]

    # No new users can be added.
    def has_add_permission(self, request):
        return False


admin.site.register(User, UserAdmin)


# Groups cannot be managed by the admin interface
admin.site.unregister(Group)

import floppyforms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import gettext_lazy as _


class WocatAuthenticationForm(floppyforms.Form, AuthenticationForm):
    username = floppyforms.CharField(
        max_length=255,
        label=_("E-mail address"),
        widget=floppyforms.TextInput(attrs={"tabindex": 1, "autofocus": True}),
    )
    password = floppyforms.CharField(
        label=_("Password"), widget=floppyforms.PasswordInput(attrs={"tabindex": 2})
    )

from django.urls import path

from . import views

app_name = "accounts"

urlpatterns = [
    path("login/", views.LoginView.as_view(), name="login"),
    path("logout/", views.logout, name="logout"),
    path("search/", views.user_search, name="user_search"),
    path("update/", views.user_update, name="user_update"),
    path("user/<int:pk>/", views.UserDetailView.as_view(), name="user_details"),
    path("questionnaires/", views.ProfileView.as_view(), name="account_questionnaires"),
    path(
        "questionnaires/status/<int:user_id>/",
        views.PublicQuestionnaireListView.as_view(),
        name="questionnaires_public_list",
    ),
    path(
        "questionnaires/status/",
        views.QuestionnaireStatusListView.as_view(),
        name="questionnaires_status_list",
    ),
    path(
        "questionnaires/search/",
        views.QuestionnaireSearchView.as_view(),
        name="staff_questionnaires_search",
    ),
]

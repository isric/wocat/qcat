from django.urls import path, include

from apps.api.views import APIRoot
from drf_spectacular.views import (
    SpectacularRedocView,
    SpectacularAPIView,
    SpectacularSwaggerView,
)

app_name = "api"

urlpatterns = [
    path("v1/", include("apps.api.urls.v1")),
    path("v2/", include("apps.api.urls.v2")),
    path("schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "docs/", SpectacularSwaggerView.as_view(url_name="api:schema"), name="api-docs"
    ),
    path("redoc/", SpectacularRedocView.as_view(url_name="api:schema")),
    path("", APIRoot.as_view(), name="api-root"),
]

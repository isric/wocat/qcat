from django.urls import include, path

from apps.questionnaire import views

urlpatterns = [
    path("all_labels/", views.all_labels),
    path("set_label/", views.set_label),
]

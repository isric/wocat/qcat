from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from apps.questionnaire.api import views

app_name = "v1"

urlpatterns = [
    path(
        "questionnaires/",
        views.QuestionnaireListView.as_view(),
        name="questionnaires-api-list",
    ),
    path(
        "questionnaires/<identifier>/",
        views.QuestionnaireDetailView.as_view(),
        name="questionnaires-api-detail",
    ),
]

urlpatterns = format_suffix_patterns(urlpatterns)

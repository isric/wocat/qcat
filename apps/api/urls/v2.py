from django.urls import path, re_path
from rest_framework.urlpatterns import format_suffix_patterns

from apps.api import views as api_views
from apps.configuration.api import views as configuration_views
from apps.questionnaire.api import views as questionnaire_views

app_name = "v2"

urlpatterns = {
    path(
        "questionnaires/",
        questionnaire_views.QuestionnaireListView.as_view(),
        name="questionnaires-api-list",
    ),
    # re_path(r'^questionnaires/(?P<identifier>[^/]+)/$',
    re_path(
        r"^questionnaires/(?P<identifier>(?!mydata)[^/]+)/$",
        questionnaire_views.ConfiguredQuestionnaireDetailView.as_view(),
        name="questionnaires-api-detail",
    ),
    path("auth/login/", api_views.ObtainAuthToken.as_view(), name="api-token-auth"),
    re_path(
        r"^questionnaires/(?P<configuration>[^/]+)/(?P<edition>[^/]+)/create/$",
        questionnaire_views.QuestionnaireCreateNew.as_view(),
        name="questionnaires-api-create",
    ),
    re_path(
        r"^questionnaires/(?P<configuration>[^/]+)/(?P<edition>[^/]+)/(?P<identifier>(?!create)[^/]+)/$",
        questionnaire_views.QuestionnaireEdit.as_view(),
        name="questionnaires-api-edit",
    ),
    path(
        "questionnaires/mydata/",
        questionnaire_views.QuestionnaireMyData.as_view(),
        name="questionnaires-api-mydata",
    ),
    path(
        "image/upload/",
        questionnaire_views.QuestionnaireImageUpload.as_view(),
        name="questionnaires-api-image-upload",
    ),
    path(
        "configuration/<code>/<edition>/",
        configuration_views.ConfigurationStructureView.as_view(),
        name="api-configuration-structure",
    ),
    path(
        "configuration/",
        configuration_views.ConfigurationView.as_view(),
        name="configuration-api-list",
    ),
    path(
        "configuration/<code>/",
        configuration_views.ConfigurationEditionView.as_view(),
        name="configuration-edition-api-list",
    ),
}

urlpatterns = format_suffix_patterns(urlpatterns)

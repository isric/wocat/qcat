from django.urls import path

from apps.questionnaire import views
from . import views as cca_views

app_name = "cca"

urlpatterns = [
    # The 'home' route points to the list
    path(
        "view/<identifier>/",
        views.QuestionnaireView.as_view(url_namespace=__package__),
        name="questionnaire_details",
    ),
    path(
        "view/<identifier>/map/",
        views.QuestionnaireMapView.as_view(url_namespace=__package__),
        name="questionnaire_view_map",
    ),
    path(
        "view/<identifier>/<step>/",
        cca_views.questionnaire_view_step,
        name="questionnaire_view_step",
    ),
    path(
        "edit/new/",
        views.QuestionnaireEditView.as_view(url_namespace=__package__),
        name="questionnaire_new",
    ),
    path(
        "edit/<identifier>/",
        views.QuestionnaireEditView.as_view(url_namespace=__package__),
        name="questionnaire_edit",
    ),
    path(
        "edit/<identifier>/<step>/",
        views.QuestionnaireStepView.as_view(url_namespace=__package__),
        name="questionnaire_new_step",
    ),
    path(
        "search/links/",
        views.QuestionnaireLinkSearchView.as_view(configuration_code=__package__),
        name="questionnaire_link_search",
    ),
]

from django.urls import path

from .views import EditionNotesView

app_name = "configuration"

urlpatterns = [
    # The 'home' route points to the list
    path("", EditionNotesView.as_view(), name="release_notes"),
]

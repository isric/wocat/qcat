from django import template
from django.utils import timezone

from apps.message.models import Message

register = template.Library()


@register.inclusion_tag("messages/messages.html")
def show_messages():
    return {
        "messages": Message.objects.filter(is_active=True).exclude(
            expires_at__lte=timezone.now()
        )
    }

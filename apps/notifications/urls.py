from django.urls import re_path, path

from . import views

urlpatterns = [
    path("", views.LogListTemplateView.as_view(), name="notification_list"),
    path("partial/", views.LogListView.as_view(), name="notification_partial_list"),
    path("partial/todo/", views.LogTodoView.as_view(), name="notification_todo_list"),
    path("read/", views.ReadLogUpdateView.as_view(), name="notification_read"),
    path("read/all/", views.LogAllReadView.as_view(), name="notification_all_read"),
    path(
        "inform-compiler/",
        views.LogInformationUpdateCreateView.as_view(),
        name="notification_inform_compiler",
    ),
    path(
        "new-notifications/",
        views.LogCountView.as_view(),
        name="notification_new_count",
    ),
    path(
        "questionnaire-logs/",
        views.LogQuestionnairesListView.as_view(),
        name="notification_questionnaire_logs",
    ),
    re_path(
        r"^preferences/(?P<token>\d+:[\w_-]+)/$",
        views.SignedLogSubscriptionPreferencesView.as_view(),
        name="signed_notification_preferences",
    ),
    path(
        "preferences/",
        views.LogSubscriptionPreferencesView.as_view(),
        name="notification_preferences",
    ),
]

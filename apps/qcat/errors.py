class ConfigurationError(Exception):
    pass


class ConfigurationErrorNoConfigurationFound(ConfigurationError):
    def __init__(self, keyword):
        self.keyword = keyword

    def __str__(self):
        return f'No active configuration found for keyword "{self.keyword}"'


class ConfigurationErrorNotInDatabase(ConfigurationError):
    def __init__(self, Model, keyword):
        self.Model = Model
        self.keyword = keyword

    def __str__(self):
        return f'No database entry found for keyword "{self.keyword}" in model {self.Model}'


class ConfigurationErrorInvalidOption(ConfigurationError):
    def __init__(self, option, configuration, object_):
        self.option = option
        self.configuration = configuration
        self.object_ = object_

    def __str__(self):
        return f'Option "{self.option}" is not valid for configuration "{self.configuration}" of object {self.object_}.'


class ConfigurationErrorInvalidCondition(ConfigurationError):
    def __init__(self, condition, error):
        self.condition = condition
        self.error = error

    def __str__(self):
        return 'Condition "{}" is not valid. Reason: "{}"'.format(
            self.condition, self.error
        )


class ConfigurationErrorInvalidQuestiongroupCondition(ConfigurationError):
    def __init__(self, condition, error):
        self.condition = condition
        self.error = error

    def __str__(self):
        return f'Questiongroup condition "{self.condition}" is not valid. Reason: "{self.error}"'


class ConfigurationErrorInvalidConfiguration(ConfigurationError):
    def __init__(self, configuration, format, parent):
        self.configuration = configuration
        self.format = format
        self.parent = parent

    def __str__(self):
        return f'Configuration "{self.configuration}" (part of "{self.parent}") is missing or is not of format "{self.format}"'


class ConfigurationErrorTemplateNotFound(ConfigurationError):
    def __init__(self, template, object_):
        self.template = template
        self.object_ = object_

    def __str__(self):
        return f'Template "{self.template}" for object "{self.object_}" not found.'


class QuestionnaireFormatError(Exception):
    def __init__(self, questionnaire_data):
        self.questionnaire_data = questionnaire_data

    def __str__(self):
        return f'The questionnaire format is invalid: "{self.questionnaire_data}"'

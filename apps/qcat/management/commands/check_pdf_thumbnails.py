import os

from django.core.management.base import BaseCommand

from apps.questionnaire.models import File
from apps.questionnaire.upload import create_thumbnails


class Command(BaseCommand):
    def handle(self, *args, **options):
        pdfs = File.objects.filter(content_type="application/pdf")
        for pdf in pdfs:
            has_issues = False
            for size, haesch in pdf.thumbnails.items():
                pth = os.path.join("media", haesch[:2], haesch[2], f"{haesch}.jpg")
                if not os.path.isfile(pth):
                    has_issues = True
            if has_issues:
                origpth = os.path.join(
                    "media", pdf.uuid[:2], pdf.uuid[2], f"{pdf.uuid}.pdf"
                )

                pdf.thumbnails = create_thumbnails(origpth, "application/pdf")
                pdf.save()

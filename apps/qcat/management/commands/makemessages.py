import os

from django.core.management.commands import makemessages
from django.db.models import Q

from apps.configuration.cache import get_configuration
from apps.configuration.models import Configuration, TranslationContent, Translation


class Command(makemessages.Command):
    msgmerge_options = [
        "-q",
        "--backup=none",
        "--previous",
        "--update",
        "--no-fuzzy-matching",
        "--no-wrap",
        "--no-location",
    ]
    msgattrib_options = ["--no-obsolete", "--no-location"]
    xgettext_options = [
        "--from-code=UTF-8",
        "--add-comments=Translators",
        "--no-location",
    ]

    def handle(self, *args, **options):
        configuration_helper_file = os.path.join(
            "apps", "configuration", "configuration_translations.py"
        )
        used_translation_ids = []
        for configuration in Configuration.objects.all():
            questionnaire_configuration = get_configuration(
                code=configuration.code, edition=configuration.edition
            )
            used_translation_ids.extend(
                questionnaire_configuration.get_translation_ids()
            )

        # Remove duplicates
        used_translation_ids = set(used_translation_ids)

        # It is not enough to check for new 'Translation' rows as new
        # configuration editions can update only the translation for the new
        # edition which creates a new entry in the existing 'Translation' data
        # json.
        # Instead, first get all existing content of TranslationContent, group
        # it by configuration and keyword. Then extract all entries in the
        # 'Translation' table and see if they are already available in the
        # 'TranslationContent' table. If not, create them later.
        existing_translation_content = {}
        for existing in TranslationContent.objects.all():
            (
                existing_translation_content.setdefault(existing.configuration, {})
                .setdefault(existing.keyword, [])
                .append(existing.text)
            )

        unused_translation_ids = []
        for translation in Translation.objects.all():
            if translation.id not in used_translation_ids:
                unused_translation_ids.append(translation.id)
                continue

            for configuration, contents in translation.data.items():
                for keyword, translated_items in contents.items():
                    # Only English texts are expected. If 'en' is not available,
                    # this must raise an exception.
                    translation_string = translated_items["en"]

                    if translation_string in existing_translation_content.get(
                        configuration, {}
                    ).get(keyword, []):
                        # String already in TranslationContent, do nothing.
                        continue

                    # Add new strings to TranslationContent
                    TranslationContent(
                        translation=translation,
                        configuration=configuration,
                        keyword=keyword,
                        text=translation_string,
                    ).save()

                    if len(translated_items.keys()) != 1:
                        print(
                            "Warning: More than one translation in the"
                            "fixtures. Only the English text is used."
                        )

        if unused_translation_ids:
            print(
                "The following translations are not needed anymore. They will "
                "be deleted. You should also remove these from the fixtures, "
                "along with the keys/values/categories they belong to (if they "
                "still exist). \n%s"
                % "\n".join([str(i) for i in sorted(unused_translation_ids)])
            )
            TranslationContent.objects.filter(
                translation__id__in=unused_translation_ids
            ).delete()
            Translation.objects.filter(id__in=unused_translation_ids).delete()

        # All translations must be written to the file again.
        # By using pgettext and contextual markers, one separate
        # translation per configuration and keyword is ensured.
        all_translations = (
            TranslationContent.objects.exclude(text="")
            .filter(
                Q(translation__category__isnull=False)
                | Q(translation__questiongroup__isnull=False)
                | Q(translation__key__isnull=False)
                | Q(translation__value__isnull=False)
            )
            .order_by("translation__id", "id")
        )
        with open(configuration_helper_file, "w") as f:
            line_number = 1
            for translation in all_translations:
                while line_number < translation.translation.id:
                    f.write("\n")
                    line_number += 1
                f.write(
                    'pgettext("{0} {1}", {2!r})\n'.format(
                        translation.configuration,
                        translation.keyword,
                        translation.text.replace("\r", "").replace("%", "%%"),
                    )
                )
                line_number += 1

        self._call_parent_makemessages(*args, **options)

        os.unlink(configuration_helper_file)

    def _call_parent_makemessages(self, *args, **options):
        # Create the .po files.
        print("Writing po files.")
        options["ignore_patterns"].extend(["node_modules/*"])
        super().handle(*args, **options)

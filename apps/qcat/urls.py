from django.urls import path
from django.views.generic import TemplateView

from apps.qcat import views

urlpatterns = [
    path("", views.about, name="about"),
    path(
        "imprint/",
        TemplateView.as_view(template_name="qcat/imprint.html"),
        name="imprint",
    ),
    path(
        "terms-of-agreement/",
        TemplateView.as_view(template_name="qcat/terms-of-agreement.html"),
        name="terms-of-agreement",
    ),
    path(
        "privacy-policy/",
        TemplateView.as_view(template_name="qcat/privacy-policy.html"),
        name="privacy-policy",
    ),
]

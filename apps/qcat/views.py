import logging
from datetime import timedelta

import requests
from django.conf import settings
from django.contrib.auth import get_user_model
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone
from django.views.generic import TemplateView
from requests.exceptions import RequestException

from apps.questionnaire.models import Questionnaire, QuestionnaireMembership

logger = logging.getLogger(__name__)


def home(request):
    return render(request, "home.html")


def about(request):
    return render(request, "about.html")


# class StaticViewSitemap(sitemaps.Sitemap):
#     """
#     Until a better solution is required: just use a static sitemap.
#     """
#
#     def items(self):
#         return [
#             "wocat:home",
#             "wocat:questionnaire_list",
#             # "login",
#         ]
#
#     def location(self, item):
#         return reverse_lazy(item)
#
#
# # Use this for the urls.
# static_sitemap = {
#     "static": StaticViewSitemap,
# }


class FactsTeaserView(TemplateView):
    """
    Display some relevant numbers.
    This is built with the idea that the date-range can be edited by the user.
    """

    http_method_names = ["get"]
    template_name = "qcat/templates/fact_sheet_teaser.html"
    start_date_offset_days = 90
    date_to = timezone.now()

    def _get_questionnaire_facts(self):
        """
        Get data about questionnaires.
        """
        return {
            "questionnaires": Questionnaire.with_status.public().count(),
            "technologies": Questionnaire.with_status.public()
            .filter(code__startswith="technologies")
            .count(),
            "approaches": Questionnaire.with_status.public()
            .filter(code__startswith="approaches")
            .count(),
            "unccd": Questionnaire.with_status.public()
            .filter(code__startswith="unccd")
            .count(),
            "new_practices_published": Questionnaire.with_status.public()
            .filter(
                updated__gte=(
                    timezone.now() - timedelta(days=self.start_date_offset_days)
                )
            )
            .count(),
            "countries": len(
                set(
                    Questionnaire.with_status.public()
                    .extra(select={"countries": "data->'qg_location'->0->'country'"})
                    .values_list("countries", flat=True)
                )
            ),
        }

    def _get_user_facts(self):
        """
        Get data about users.
        """
        return {
            "compilers": QuestionnaireMembership.objects.filter(
                questionnaire__status=settings.QUESTIONNAIRE_PUBLIC, role="compiler"
            )
            .distinct("user")
            .count(),
            "users": get_user_model().objects.all().count(),
        }

    def get_context_data(self, **kwargs) -> dict:
        """
        Combine all kinds of facts.
        """
        context = super().get_context_data(**kwargs)
        context["days"] = self.start_date_offset_days
        context.update(**self._get_questionnaire_facts())
        context.update(**self._get_user_facts())
        return context


class AdminLoginView(TemplateView):
    def get(self, request, *args, **kwargs):
        """
        Assuming the name of the external system's login url is "login"
        """
        return HttpResponseRedirect(reverse("accounts:login"))

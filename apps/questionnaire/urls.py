from django.urls import include, path

from . import views

urlpatterns = [
    path("upload/", views.generic_file_upload, name="file_upload"),
    path("file/<action>/<uid>/", views.generic_file_serve, name="file_serve"),
    path(
        "edit/<identifier>/lock/",
        views.QuestionnaireLockView.as_view(),
        name="lock_questionnaire",
    ),
    path("geo/", views.get_places, name="slm_places"),
    path("update/", views.get_places, name="update_slm_places"),
    path("", include("apps.sample.urls")),
]

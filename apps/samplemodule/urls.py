from django.urls import path

from apps.questionnaire import views
from . import views as samplemodule_views

app_name = "samplemodule"

urlpatterns = [
    path(
        "view/<identifier>/",
        views.QuestionnaireView.as_view(url_namespace=__package__),
        name="questionnaire_details",
    ),
    path(
        "view/<identifier>/<step>/",
        samplemodule_views.questionnaire_view_step,
        name="questionnaire_view_step",
    ),
    path(
        "edit/new/",
        views.QuestionnaireEditView.as_view(url_namespace=__package__),
        name="questionnaire_new",
    ),
    path(
        "edit/<identifier>/",
        views.QuestionnaireEditView.as_view(url_namespace=__package__),
        name="questionnaire_edit",
    ),
    path(
        "edit/<identifier>/<step>/",
        views.QuestionnaireStepView.as_view(url_namespace=__package__),
        name="questionnaire_new_step",
    ),
]

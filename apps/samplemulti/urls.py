from django.urls import path
from django.views.generic import TemplateView

from apps.questionnaire import views
from . import views as samplemulti_views

app_name = "samplemulti"

urlpatterns = [
    path("", TemplateView.as_view(template_name="samplemulti/home.html"), name="home"),
    path(
        "view/<identifier>/",
        views.QuestionnaireView.as_view(url_namespace=__package__),
        name="questionnaire_details",
    ),
    path(
        "view/<identifier>/<step>/",
        samplemulti_views.questionnaire_view_step,
        name="questionnaire_view_step",
    ),
    path(
        "edit/new/",
        views.QuestionnaireEditView.as_view(url_namespace=__package__),
        name="questionnaire_new",
    ),
    path(
        "edit/<identifier>/",
        views.QuestionnaireEditView.as_view(url_namespace=__package__),
        name="questionnaire_edit",
    ),
    path(
        "edit/<identifier>/<step>/",
        views.QuestionnaireStepView.as_view(url_namespace=__package__),
        name="questionnaire_new_step",
    ),
    path(
        "search/links/",
        views.QuestionnaireLinkSearchView.as_view(configuration_code=__package__),
        name="questionnaire_link_search",
    ),
    path(
        "list/",
        views.QuestionnaireListView.as_view(configuration_code=__package__),
        name="questionnaire_list",
    ),
    path(
        "list_partial/",
        views.QuestionnaireListView.as_view(configuration_code=__package__),
        name="questionnaire_list_partial",
    ),
]

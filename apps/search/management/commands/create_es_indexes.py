from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils.translation import activate

from apps.configuration.cache import get_configuration
from apps.configuration.models import Configuration
from apps.search.index import get_mappings, create_or_update_index


class Command(BaseCommand):
    """
    This command creates opensearchpy indexes for all available
    questionnaires. This is primarily used for running tests on shippable.
    """

    def add_arguments(self, parser):
        parser.add_argument("language", nargs="*", type=str)

    def handle(self, **options):
        configurations = Configuration.objects.all()
        if options.get("language"):
            languages = options["language"]
        else:
            languages = list(dict(settings.LANGUAGES).keys())
        print("creating es indexes for ", languages)
        for language in languages:
            activate(language)
            for configuration in configurations:
                questionnaire_configuration = get_configuration(
                    code=configuration.code, edition=configuration.edition
                )
                mappings = get_mappings()
                create_or_update_index(
                    configuration=questionnaire_configuration, mappings=mappings
                )

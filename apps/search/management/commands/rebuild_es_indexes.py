from django.core.management import call_command
from django.core.management.base import BaseCommand

from apps.questionnaire.models import Questionnaire
from apps.search.index import delete_all_indices, put_questionnaire_data


class Command(BaseCommand):
    """
    Delete, recreate and fill all indexes.
    """

    def handle(self, **options):
        delete_all_indices()
        call_command("create_es_indexes")
        put_questionnaire_data(
            questionnaire_objects=Questionnaire.with_status.public(), request_timeout=60
        )

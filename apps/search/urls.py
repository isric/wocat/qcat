from django.urls import path

from apps.configuration import views as configuration_views
from . import views

app_name = "search"

urlpatterns = [
    path("admin/", views.admin, name="admin"),
    path("delete/", views.delete_all, name="delete_all"),
    path("delete/<configuration>/<edition>/", views.delete_one, name="delete_one"),
    path("index/<configuration>/<edition>/", views.index, name="index"),
    path("update/<configuration>/<edition>/", views.update, name="update"),
    # TODO This does not necessarily belong here
    path("cache/delete/", configuration_views.delete_caches, name="delete_caches"),
    path(
        "cache/build/",
        configuration_views.BuildAllCachesView.as_view(),
        name="build_caches",
    ),
    path("value/", views.FilterValueView.as_view(), name="filter_value"),
]

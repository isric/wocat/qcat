from django.urls import path

from .views import SummaryPDFCreateView

urlpatterns = [
    path("<int:id>/", SummaryPDFCreateView.as_view(), name="questionnaire_summary"),
]

from django.contrib.auth.decorators import login_required
from django.urls import path
from django.views.generic import TemplateView

from apps.questionnaire import views
from . import views as technologies_views

app_name = "technologies"

urlpatterns = [
    # The 'home' route points to the list
    path(
        "view/<identifier>/",
        views.QuestionnaireView.as_view(url_namespace=__package__),
        name="questionnaire_details",
    ),
    path(
        "view/permalink/<pk>/",
        views.QuestionnairePermaView.as_view(url_namespace=__package__),
        name="questionnaire_permalink",
    ),
    path(
        "view/<identifier>/map/",
        views.QuestionnaireMapView.as_view(url_namespace=__package__),
        name="questionnaire_view_map",
    ),
    path(
        "view/<identifier>/<step>/",
        technologies_views.questionnaire_view_step,
        name="questionnaire_view_step",
    ),
    path(
        "edit/new/",
        views.QuestionnaireEditView.as_view(url_namespace=__package__),
        name="questionnaire_new",
    ),
    path(
        "edit/<identifier>/",
        views.QuestionnaireEditView.as_view(url_namespace=__package__),
        name="questionnaire_edit",
    ),
    path(
        "edit/<identifier>/<step>/",
        views.QuestionnaireStepView.as_view(url_namespace=__package__),
        name="questionnaire_new_step",
    ),
    path(
        "search/links/",
        views.QuestionnaireLinkSearchView.as_view(configuration_code=__package__),
        name="questionnaire_link_search",
    ),
    path(
        "add_module/",
        login_required(
            TemplateView.as_view(template_name="technologies/add_module.html")
        ),
        name="add_module",
    ),
    path(
        "add_module_action/",
        views.QuestionnaireAddModule.as_view(url_namespace=__package__),
        name="add_module_action",
    ),
    path(
        "check_modules/",
        views.QuestionnaireCheckModulesView.as_view(),
        name="check_modules",
    ),
]

from django.urls import reverse_lazy, path
from django.views.generic import RedirectView

from apps.questionnaire import views
from . import views as unccd_views

app_name = "unccd"

urlpatterns = [
    path(
        "",
        RedirectView.as_view(url=reverse_lazy("wocat:home"), permanent=False),
        name="home",
    ),
    path(
        "view/<identifier>/",
        views.QuestionnaireView.as_view(url_namespace=__package__),
        name="questionnaire_details",
    ),
    path(
        "view/<identifier>/<step>/",
        unccd_views.questionnaire_view_step,
        name="questionnaire_view_step",
    ),
    path(
        "edit/new/",
        views.QuestionnaireEditView.as_view(url_namespace=__package__),
        name="questionnaire_new",
    ),
    path(
        "edit/<identifier>/",
        views.QuestionnaireEditView.as_view(url_namespace=__package__),
        name="questionnaire_edit",
    ),
    path(
        "edit/<identifier>/<step>/",
        views.QuestionnaireStepView.as_view(url_namespace=__package__),
        name="questionnaire_new_step",
    ),
    path("import/", unccd_views.unccd_data_import, name="data_import"),
]

from django.urls import path
from django.views.generic import TemplateView

from apps.questionnaire.views import QuestionnaireListView, QuestionnaireFilterView

app_name = "wocat"

urlpatterns = [
    path("", TemplateView.as_view(template_name="wocat/home.html"), name="home"),
    path(
        "help/questionnaire/",
        TemplateView.as_view(
            template_name="wocat/help/questionnaire_introduction.html"
        ),
        name="help_questionnaire_introduction",
    ),
    path("faq/", TemplateView.as_view(template_name="wocat/faq.html"), name="faq"),
    path("add/", TemplateView.as_view(template_name="wocat/add.html"), name="add"),
    path(
        "list/",
        QuestionnaireListView.as_view(configuration_code=__package__),
        name="questionnaire_list",
    ),
    path(
        "list_partial/",
        QuestionnaireListView.as_view(configuration_code=__package__),
        name="questionnaire_list_partial",
    ),
    path(
        "filter/",
        QuestionnaireFilterView.as_view(configuration_code=__package__),
        name="questionnaire_filter",
    ),
    path(
        "filter_partial/",
        QuestionnaireFilterView.as_view(configuration_code=__package__),
        name="questionnaire_filter_partial",
    ),
    # path("", include(("apps.accounts.urls", "accounts"), namespace="accounts")),
    # path("", include(("apps.approaches.urls", "approaches"), namespace="approaches")),
    # path("", include(("apps.sample.urls", "sample"), namespace="sample")),
    # path("", include(("apps.api.urls.v1", "v1"), namespace="v1")),
    # path(
    #     "", include(("apps.samplemulti.urls", "samplemulti"), namespace="samplemulti")
    # ),
    # path(
    #     "",
    #     include(("apps.samplemodule.urls", "samplemodule"), namespace="samplemodule"),
    # ),
]

from django.contrib.messages import constants as messages
from os.path import join

import environ
from django.utils.translation import gettext_lazy as _

ENV_PATH = "./.env"
BASE_DIR: environ.Path = environ.Path(__file__) - 3
env = environ.Env()
env.read_env(BASE_DIR(ENV_PATH))

BASE_URL = "https://qcat.wocat.net"

INSTALLED_APPS = [
    "django.contrib.gis",
    "django.contrib.contenttypes",
    "grappelli",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.sessions",
    "django.contrib.messages",
    # "django.contrib.sitemaps",
    "django.contrib.staticfiles",
    "django.contrib.humanize",
    "corsheaders",
    "django_filters",
    "easy_thumbnails",
    "easy_thumbnails.optimize",
    "floppyforms",
    "imagekit",
    "rest_framework",
    "drf_spectacular",
    # Leaflet
    "leaflet",
    # Custom apps
    "apps.accounts",
    "apps.api",
    "apps.approaches",
    "apps.configuration",
    "apps.qcat",
    "apps.questionnaire",
    "apps.notifications",
    "apps.sample",
    "apps.samplemulti",
    "apps.samplemodule",
    "apps.search",
    "apps.summary",
    "apps.technologies",
    "apps.unccd",
    "apps.watershed",
    "apps.wocat",
    "apps.cca",
    "apps.cbp",
    "apps.message",
]

LANGUAGE_CODE = "en"
LANGUAGES = (
    ("en", _("English")),
    ("fr", _("French")),
    ("es", _("Spanish")),
    ("ru", _("Russian")),
    ("zh", _("Chinese")),
    ("km", _("Khmer")),
    ("lo", _("Lao")),
    ("ar", _("Arabic")),
    ("pt", _("Portuguese")),
    ("af", _("Afrikaans")),
    ("th", _("Thai")),
    ("mn", _("Mongolian")),
)
WORD_WRAP_LANGUAGES = ["km", "lo", "th"]
TIME_ZONE = "Europe/Zurich"
USE_I18N = True
USE_L10N = True
USE_TZ = True
# SITE_ID = 1
# SITE_PROTOCOL = "https"
#
# # https://django-environ.readthedocs.io/en/latest/types.html?highlight=smtp#environ-env-email-url
EMAIL_CONFIG = env.email_url("DJANGO_EMAIL_URL", default="consolemail://")
vars().update(EMAIL_CONFIG)
# SERVER_EMAIL = EMAIL_CONFIG["EMAIL_HOST_USER"]
DEFAULT_FROM_EMAIL = env(
    "DJANGO_DEFAULT_FROM_EMAIL", default="Wocat NOREPLY <wocat-noreply@wocat.net>"
)
DO_SEND_EMAILS = env.bool("DO_SEND_EMAILS", default=True)
DO_SEND_STAFF_ONLY = env.bool("DO_SEND_STAFF_ONLY", default=False)

ALLOWED_HOSTS = env.list("DJANGO_ALLOWED_HOSTS", default=["127.0.0.1", "localhost"])

ROOT_URLCONF = "config.urls"

WSGI_APPLICATION = "config.wsgi.application"

DATABASES = {"default": env.db("DJANGO_DATABASE_URL")}
DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

LOCALE_PATHS = [BASE_DIR("config/locale")]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    # "apps.qcat.middleware.ProfilerMiddleware",
]

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [BASE_DIR("templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
                "apps.qcat.context_processors.template_settings",
            ],
        },
    }
]

STATIC_ROOT = BASE_DIR("../static-collected")
STATIC_URL = "/static/"
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]
STATICFILES_STORAGE = "whitenoise.storage.CompressedStaticFilesStorage"
STATICFILES_DIRS = [
    BASE_DIR("static"),
    BASE_DIR("node_modules/parallax-js/deploy"),
    BASE_DIR("node_modules/leaflet.markercluster/dist"),
    BASE_DIR("node_modules/jquery/dist"),
    BASE_DIR("node_modules/jquery-nstslider/dist"),
    # BASE_DIR("node_modules/foundation-sites/dist"),
    BASE_DIR("node_modules/zurb-foundation-5/js/foundation"),
    BASE_DIR("node_modules/slick-carousel/slick"),
    BASE_DIR("node_modules/intro.js/minified"),
    BASE_DIR("node_modules/chosen-js"),
    BASE_DIR("node_modules/dropzone/dist/min/"),
]

MEDIA_ROOT = BASE_DIR("media/")
MEDIA_URL = "media/"  # normally "/media/"
UPLOAD_MAX_FILE_SIZE = 104857600000  # 100 MB
UPLOAD_VALID_FILES = {
    "image": (
        ("image/jpeg", "jpg"),
        ("image/png", "png"),
        ("image/gif", "gif"),
    ),
    "document": (("application/pdf", "pdf"),),
}
UPLOAD_IMAGE_THUMBNAIL_FORMATS = (
    ("default", (640, 480)),
    ("small", (1024, 768)),
    ("medium", (1440, 1080)),
    # 'large' is the original uploaded image.
)
THUMBNAIL_ALIASES = {
    "summary": {
        "screen": {
            "header_image": {
                "size": (1542, 767),
                "upscale": True,
                "crop": True,
                "target": (50, 50),
            },
            "half_height": {
                "size": (640, 640),
                "upscale": True,
                "crop": True,
            },
            "map": {"size": (560, 0)},
            "flow_chart": {"size": (900, 0), "upscale": False},
            "flow_chart_half_height": {
                "size": (640, 640),
                "upscale": True,
            },
        },
        "print": {
            "header_image": {
                "size": (6168, 3068),
                "crop": True,
                "upscale": True,
            },
            "half_height": {
                "size": (2560, 2560),
                "upscale": True,
                "crop": True,
            },
            "map": {"size": (2240, 0)},
            "flow_chart": {"size": (3600, 0), "upscale": False},
            "flow_chart_half_height": {
                "size": (2560, 2560),
                "upscale": True,
            },
        },
    }
}
SUMMARY_PDF_PATH = join(MEDIA_ROOT, "summary-pdf")

# FILE_UPLOAD_PERMISSIONS = 0o2755
# FILE_UPLOAD_MAX_MEMORY_SIZE = 50000000
#
# AUTH_PASSWORD_VALIDATORS = [
#     {
#         "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
#     },
#     {
#         "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
#         "OPTIONS": {"min_length": 9},
#     },
#     {
#         "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
#     },
#     {
#         "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
#     },
# ]
#
AUTH_USER_MODEL = "accounts.User"
AUTHENTICATION_BACKENDS = ["apps.accounts.authentication.WocatCMSAuthenticationBackend"]
REACTIVATE_WOCAT_ACCOUNT_URL = "https://wocat.net/accounts/reactivate/"
LOGIN_URL = "accounts:login"


LEAFLET_CONFIG = {
    "DEFAULT_CENTER": (6.0, 45.0),
    "DEFAULT_ZOOM": 2,
    "MIN_ZOOM": 2,
    "MAX_ZOOM": 16,
    "DEFAULT_PRECISION": 6,
    "TILES": [
        (
            "Esri World Imagery",
            "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
            {
                "attribution": "Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012",
                "maxZoom": 16,
            },
        ),
        (
            "Esri WorldStreet Map",
            "https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}",
            {
                "attribution": "Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012",
                "maxZoom": 16,
            },
        ),
        (
            "Esri WorldTopo Map",
            "https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}",
            {
                "attribution": "Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012",
                "maxZoom": 16,
            },
        ),
    ],
}

# Elasticsearch settings
ES_HOST = env("ES_HOST", default="elasticsearch")
ES_PORT = 9200
ES_USE_SSL = env.bool("ES_USE_SSL", default=True)
ES_HTTP_AUTH = env.tuple("ES_HTTP_AUTH", default=("admin", "admin"))
ES_INDEX_PREFIX = "qcat_"
ES_NESTED_FIELDS_LIMIT = 250
# curl -X GET https://localhost:9200 -u 'admin:admin' --insecure
# For each language (as set in the setting ``LANGUAGES``), a language
# analyzer can be specified. This helps to analyze the text in the
# corresponding language for better search results.
# https://www.elastic.co/guide/en/elasticsearch/reference/1.6/analysis-lang-analyzer.html  # noqa
ES_ANALYZERS = (("en", "english"), ("es", "spanish"))
# https://www.elastic.co/guide/en/elasticsearch/reference/2.0/query-dsl-query-string-query.html#_reserved_characters
ES_QUERY_RESERVED_CHARS = [
    "\\",
    "+",
    "-",
    "=",
    "&&",
    "||",
    ">",
    "<",
    "!",
    "(",
    ")",
    "{",
    "}",
    "[",
    "]",
    "^",
    '"',
    "~",
    "*",
    "?",
    ":",
    "/",
]


MESSAGE_TAGS = {messages.INFO: "secondary"}

# The base URL of the REST API used for authentication
AUTH_API_URL = env("AUTH_API_URL", default="https://www.wocat.net/api/v1/")
AUTH_API_USER = env("AUTH_API_USER", default="")
AUTH_API_KEY = env("AUTH_API_KEY", default="")
AUTH_API_TOKEN = env("AUTH_API_TOKEN", default="")
AUTH_LOGIN_FORM = env(
    "AUTH_LOGIN_FORM", default="https://dev.wocat.net/en/sitefunctions/login.html"
)

# Settings for piwik integration. Tracking happens in the frontend
# (base template) and backend (API)
PIWIK_SITE_ID = env("PIWIK_SITE_ID", default=None)
PIWIK_URL = "https://webstats.wocat.net/"
PIWIK_AUTH_TOKEN = env("PIWIK_AUTH_TOKEN", default="")
PIWIK_API_VERSION = 1

CORS_ORIGIN_WHITELIST = env.list("CORS_ORIGIN_WHITELIST", default=[])


SWAGGER_SETTINGS = {"DOC_EXPANSION": "list", "JSON_EDITOR": True, "VALIDATOR_URL": None}
API_PAGE_SIZE = 25
AUTH_COOKIE_NAME = "fe_typo_user"

# ##### ZU BEARBEITEN

# https://raw.githubusercontent.com/SeleniumHQ/selenium/master/py/CHANGES
# for the latest supported firefox version.
TESTING_FIREFOX_PATH = env("TESTING_FIREFOX_PATH", default="")
TESTING_CHROMEDRIVER_PATH = env(
    "TESTING_CHROMEDRIVER_PATH", default="/usr/local/bin/chromedriver"
)
TESTING_POP_BROWSER = env.bool("TESTING_POP_BROWSER", default=False)

USE_CACHING = env.bool("USE_CACHING", default=True)
# django-cache-url doesn't support the redis package of our choice, set the redis location as
# common environment (dict)value.
KEY_PREFIX = ""

CACHES = {"default": env.cache_url("DJANGO_CACHE_URL", default="dummycache://")}

# "Feature toggles"
IS_ACTIVE_FEATURE_MODULE = env.bool("IS_ACTIVE_FEATURE_MODULE", default=True)
IS_ACTIVE_FEATURE_WATERSHED = env.bool("IS_ACTIVE_FEATURE_WATERSHED", default=False)
IS_ACTIVE_FEATURE_MEMORY_PROFILER = env.bool(
    "IS_ACTIVE_FEATURE_MEMORY_PROFILER", default=False
)

GOOGLE_WEBMASTER_TOOLS_KEY = env("GOOGLE_WEBMASTER_TOOLS_KEY", default="")
GOOGLE_MAPS_JAVASCRIPT_API_KEY = env("GOOGLE_MAPS_JAVASCRIPT_API_KEY", default="")

WOCAT_IMPORT_DATABASE_URL = env("WOCAT_IMPORT_DATABASE_URL", default="")
WOCAT_IMPORT_DATABASE_URL_LOCAL = env("WOCAT_IMPORT_DATABASE_URL_LOCAL", default="")

WOCAT_MAILBOX_USER_ID = env.int("WOCAT_MAILBOX_USER_ID", default="")

# CDE_SUBNET_ADDR = env("CDE_SUBNET_ADDR", default="0.0.0.")

# Prevent error when submitting very large forms. Default is 1000.
# (https://docs.djangoproject.com/en/2.0/ref/settings/#data-upload-max-number-fields)
DATA_UPLOAD_MAX_NUMBER_FIELDS = 2000

SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
USE_X_FORWARDED_HOST = True

CSRF_TRUSTED_ORIGINS = [
    "https://qcat-prod.wocat.net",
    "https://qcat-dev.wocat.net",
    "https://qcat.wocat.net",
]

FILTER_EDITIONS = env.json("FILTER_EDITIONS", default={})

REST_FRAMEWORK = {
    # "DEFAULT_PERMISSION_CLASSES": ["rest_framework.permissions.IsAuthenticated"],
    # "DEFAULT_THROTTLE_RATES": {"user": "60/m"},
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
}
SPECTACULAR_SETTINGS = {
    "TITLE": "QCAT API Docs",
    "DESCRIPTION": """<p>Welcome to the documentation for the QCAT API. The API is built on top of the <a href="https://www.django-rest-framework.org/" target="_blank">Django REST framework</a>.</p>
  <p>All available endpoints of the current API version are listed below. You can expand the endpoints to see additional information and even try out the API requests.</p>
  <p>More information on the API and the structure of the returned objects, as well as code and usage examples can be found in the <a href="https://qcat.readthedocs.io/en/latest/api/docs.html">QCAT Documentation</a>.</p>""",
    "VERSION": "2.0.0",
    "SERVE_INCLUDE_SCHEMA": False,
    # "DEFAULT_GENERATOR_CLASS": "drf_spectacular.generators.SchemaGenerator",
    # "VERSION": "v2",
}

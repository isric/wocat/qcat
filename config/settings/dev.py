# flake8: noqa
from .base import *

DEBUG = env.bool("DJANGO_DEBUG", default=True)
SECRET_KEY = env("DJANGO_SECRET_KEY", default="CHANGEME")
ALLOWED_HOSTS = ["*"]
INTERNAL_IPS = ("127.0.0.1",)

try:
    import django_extensions

    INSTALLED_APPS += ["django_extensions"]
except ModuleNotFoundError:
    print("not using django_extensions...")
    pass

THUMBNAIL_DEBUG = True

# INSTALLED_APPS += ["wagtail.contrib.styleguide"]
INSTALLED_APPS += ["debug_toolbar"]
MIDDLEWARE += ["debug_toolbar.middleware.DebugToolbarMiddleware"]

# flake8: noqa
import random
import string

import sentry_sdk
from environ import ImproperlyConfigured
from sentry_sdk import serializer
from sentry_sdk.integrations.django import DjangoIntegration

from .base import *

DEBUG = False

# ManifestStaticFilesStorage is recommended in production, to prevent outdated
# JavaScript / CSS assets being served from cache (e.g. after a Wagtail upgrade).
# See https://docs.djangoproject.com/en/4.1/ref/contrib/staticfiles/#manifeststaticfilesstorage
# STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"
# TODO
# STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

try:
    SECRET_KEY = env("DJANGO_SECRET_KEY")
except ImproperlyConfigured:
    SECRET_KEY = "".join(
        [
            random.SystemRandom().choice(
                "{}{}{}".format(string.ascii_letters, string.digits, "+-:$;<=>?@^_~")
            )
            for i in range(63)
        ]
    )
    with open(ENV_PATH, "a") as envfile:
        envfile.write(f"DJANGO_SECRET_KEY={SECRET_KEY}\n")

sentry_sdk.init(
    dsn=env("DJANGO_SENTRY_DSN"),
    integrations=[DjangoIntegration()],
    send_default_pii=True,
)
serializer.MAX_DATABAG_BREADTH = 30


# THUMBNAIL_OPTIMIZE_COMMAND = {
#     "png": "/usr/bin/optipng {filename}",
#     "gif": "/usr/bin/optipng {filename}",
#     "jpeg": "/usr/bin/jpegoptim {filename}",
# }

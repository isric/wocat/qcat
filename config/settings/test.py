# flake8: noqa
from .base import *

IS_TEST_RUN = True

# LANGUAGE_CODE = "en"
# LANGUAGES = (("en", "English"),    ("es", "Spanish"),)
THUMBNAIL_DEBUG = True

SECRET_KEY = env("DJANGO_SECRET_KEY", default="CHANGEME")

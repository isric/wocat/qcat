from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import reverse_lazy, path, include
from django.views.generic import TemplateView, RedirectView

from apps.qcat.views import AdminLoginView, FactsTeaserView

urlpatterns = [
    path("about/", include("apps.qcat.urls")),
    path("grappelli/", include("grappelli.urls")),
    path("admin/login/", AdminLoginView.as_view(), name="admin_login"),
    path("admin/", admin.site.urls),
    # path("sitemap.xml", sitemap, {"sitemaps": views.static_sitemap}, name="django.contrib.sitemaps.views.sitemap",),
    path("robots.txt", TemplateView.as_view(template_name="robots.txt")),
    path("api/temp1/", include("apps.api.urls.temp")),
]
# en/questionnaire
urlpatterns += i18n_patterns(
    path(
        "",
        RedirectView.as_view(url=reverse_lazy("wocat:home"), permanent=False),
        name="home",
    ),
    path("accounts/", include("apps.accounts.urls")),
    path("api/", include("apps.api.urls")),
    path("configuration/", include("apps.configuration.urls")),
    path("notifications/", include("apps.notifications.urls")),
    path("qcat/facts_teaser", FactsTeaserView.as_view(), name="facts_teaser"),
    path("questionnaire/", include("apps.questionnaire.urls")),
    path("search/", include("apps.search.urls")),
    path("samplemulti/", include("apps.samplemulti.urls")),
    path("summary/", include("apps.summary.urls")),
    path("unccd/", include("apps.unccd.urls")),
    path("wocat/", include("apps.wocat.urls")),
    path("wocat/approaches/", include("apps.approaches.urls")),
    path("wocat/cca/", include("apps.cca.urls")),
    path("wocat/cbp/", include("apps.cbp.urls")),
    path("wocat/technologies/", include("apps.technologies.urls")),
    path("wocat/watershed/", include("apps.watershed.urls")),
)

if settings.DEBUG:
    urlpatterns += [path("__debug__/", include("debug_toolbar.urls"))]

urlpatterns += i18n_patterns(
    path("samplemodule/", include("apps.samplemodule.urls")),
    path("404/", TemplateView.as_view(template_name="404.html")),
    path("500/", TemplateView.as_view(template_name="500.html")),
    path("503/", TemplateView.as_view(template_name="503.html")),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

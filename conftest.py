import os
import pytest
from django.utils.translation import activate

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.test")


@pytest.fixture
def es(request):
    """
    In order to allow multiple Elasticsearch indices when running tests in
    parallel, overwrite ES_INDEX_PREFIX settings for each test function
    according to its slave id.

    Usage for tests that require Elasticsearch:
    @pytest.mark.usefixtures('es')
    """
    from django.conf import settings
    from apps.search.search import get_indices_alias

    # Clear lru_cache of Elasticsearch indices.
    get_indices_alias.cache_clear()

    # Test setup
    xdist_suffix = getattr(request.config, "slaveinput", {}).get(
        "slaveid", "es_test_index"
    )
    es_prefix = f"{settings.ES_INDEX_PREFIX}{xdist_suffix}"
    setattr(settings, "ES_INDEX_PREFIX", es_prefix)

    # Actual test
    yield

    # Test teardown
    # get_elasticsearch().indices.delete(index=f'{es_prefix}*')


@pytest.fixture(scope="session", autouse=True)
def clear_configuration_cache():
    from apps.configuration.cache import get_cached_configuration

    get_cached_configuration.cache_clear()


@pytest.fixture(autouse=True)
def set_default_language():
    activate("en")

#!/usr/bin/env python3
from glob import glob

import environ

# from doit import get_var

env = environ.Env()
env.read_env(".env")

DOIT_CONFIG = {"default_tasks": ["update"], "verbosity": 2}


def task_update():
    return {
        "task_dep": ["collectstatic", "compilemessages", "migrate"],
        "actions": [],
    }


#############################
def task_frontend_build():
    actions = ["npm ci; npm run build_css"]
    return {"actions": actions}


def task_collectstatic():
    return {
        "task_dep": ["frontend_build"],
        "actions": ["python manage.py collectstatic --noinput"],
    }


def task_compilemessages():
    return {"actions": ["python manage.py compilemessages"]}


def task_migrate():
    return {"actions": ["python manage.py migrate --noinput"]}

from django.conf import settings
from django.contrib.auth import get_user_model
from django.urls import reverse
from model_bakery import baker
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

from apps.notifications.models import MailPreferences
from functional_tests.base import FunctionalTest


class MailPreferencesTest(FunctionalTest):
    def setUp(self):
        super().setUp()
        self.user = baker.make(get_user_model(), firstname="jay")
        self.obj = self.user.mailpreferences

    def test_signed_preferences(self):
        # jay opens the link with the signed url stated in the mail.
        signed_url = self.live_server_url + str(self.obj.get_signed_url())
        self.browser.get(signed_url)
        # no spam please - change the value in the subscription box
        select = Select(self.findBy(By.ID, "id_subscription"))
        select.select_by_value("none")
        # submit the form
        elm = self.findBy(By.XPATH, '//input[@type="submit"]')
        self.browser.execute_script("arguments[0].click();", elm)
        # the success message is shown
        self.wait_for(By.CLASS_NAME, "notification")
        self.assertEqual(self.browser.current_url, signed_url)
        select = Select(self.findBy(By.ID, "id_subscription"))
        self.assertEqual(
            select.first_selected_option.get_attribute("value"),
            settings.NOTIFICATIONS_NO_MAILS,
        )

    def test_user_preferences(self):
        unsigned_url = self.live_server_url + reverse("notification_preferences")
        self.browser.get(unsigned_url)
        # ah, a login is required
        self.assertEqual(self.findBy(By.CLASS_NAME, "is-title").text, "Login")
        self.doLogin(user=self.user)
        # after the login, the preferences are shown
        self.browser.get(unsigned_url)
        # without changing anything, the form is submitted
        elm = self.findBy(By.XPATH, '//input[@type="submit"]')
        self.browser.execute_script("arguments[0].click();", elm)
        # and still, the url without signed id is used.
        self.assertEqual(self.browser.current_url, unsigned_url)
        # jay now changes the language and saves again.
        select = Select(self.findBy(By.ID, "id_language"))
        select.select_by_value("es")
        elm = self.findBy(By.XPATH, '//input[@type="submit"]')
        self.browser.execute_script("arguments[0].click();", elm)
        # after the success message is shown, the flag is set that the language
        # is not overridden anymore.
        self.wait_for(By.CLASS_NAME, "notification")
        self.assertTrue(
            MailPreferences.objects.get(user__firstname="jay").has_changed_language
        )

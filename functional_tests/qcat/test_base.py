from selenium.webdriver.common.by import By

from functional_tests.base import FunctionalTest


class BaseTemplateTest(FunctionalTest):
    def test_factsheet(self):
        # jay opens the start page
        self.browser.get(self.live_server_url)

        # the home-keynumbers-list is shown after a while, and shows 6
        # entries.
        self.wait_for(By.CLASS_NAME, "home-keynumbers-list")
        keynumbers_list = self.findBy(By.CLASS_NAME, "home-keynumbers-list")
        self.assertEqual(len(keynumbers_list.find_elements(By.TAG_NAME, "li")), 5)

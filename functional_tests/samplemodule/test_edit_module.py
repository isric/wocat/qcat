from selenium.webdriver.common.by import By

from apps.accounts.models import User
from django.urls import reverse
from django.test.utils import override_settings

from functional_tests.base import FunctionalTest
from apps.sample.tests.test_views import route_questionnaire_new
from selenium.webdriver.support.select import Select

route_add_module = "sample:add_module"


@override_settings(IS_ACTIVE_FEATURE_MODULE=True)
class EditModuleTest(FunctionalTest):
    fixtures = [
        "groups_permissions",
        "global_key_values",
        "sample",
        "samplemodule",
    ]

    def test_edit_module(self):
        # Alice logs in
        self.doLogin()

        # She creates a sample questionnaire
        self.browser.get(self.live_server_url + reverse(route_questionnaire_new))
        self.click_edit_section("cat_1")
        self.findBy(By.NAME, "qg_1-0-original_key_1").send_keys("Foo")
        self.findBy(By.NAME, "qg_1-0-original_key_3").send_keys("Bar")
        elm = self.findBy(By.ID, "id_qg_3-0-key_4")
        self.browser.execute_script("arguments[0].style.display='block';", elm)
        select = Select(elm)
        select.select_by_value("country_2")
        elm = self.findBy(By.ID, "button-submit")
        self.browser.execute_script("arguments[0].click();", elm)
        self.findBy(By.XPATH, '//div[contains(@class, "success")]')

        sample_url = self.browser.current_url

        # She adds a module for this questionnaire (toggle section manually)
        elm = self.findBy(
            By.XPATH, '(//a[contains(@class, "js-expand-all-sections")])[2]'
        )
        self.browser.execute_script("arguments[0].click();", elm)
        self.wait_for(
            By.XPATH, '//a[contains(@class, "js-show-embedded-modules-form")]'
        )
        elm = self.findBy(
            By.XPATH, '//a[contains(@class, "js-show-embedded-modules-form")]'
        )
        self.browser.execute_script("arguments[0].click();", elm)
        samplemodule_radio = self.findBy(
            By.XPATH, '//input[@value="samplemodule" and @name="module"]'
        )

        self.browser.execute_script("arguments[0].click();", samplemodule_radio)
        elm = self.findBy(By.XPATH, '//input[@type="submit" and @value="Create"]')
        self.browser.execute_script("arguments[0].click();", elm)
        self.findBy(By.XPATH, '//div[contains(@class, "success")]')

        module_url = self.browser.current_url

        # She sees the inherited values
        self.toggle_all_sections()
        self.findBy(By.XPATH, '//*[text()[contains(.,"Key 1 (Samplemodule)")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"Foo")]]')
        self.findByNot(By.XPATH, '//*[text()[contains(.,"Bar")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"Key 4 (Samplemodule)")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"Germany")]]')

        # She edits the first step of the module
        self.click_edit_section("modcat_1")

        # She sees the inherited values as disabled fields
        key_1 = self.findBy(By.NAME, "modqg_sample_01-0-original_key_1")
        self.assertEqual(key_1.get_attribute("value"), "Foo")
        self.assertEqual(key_1.get_attribute("disabled"), "true")

        key_4 = self.findBy(By.NAME, "modqg_sample_02-0-key_4")
        self.assertEqual(key_4.get_attribute("disabled"), "true")
        key_4_selected = self.findBy(
            By.XPATH, '//select[@name="modqg_sample_02-0-key_4"]/option[@selected]'
        )
        self.assertEqual(key_4_selected.get_attribute("value"), "country_2")

        # She sees she can edit the first question
        modkey_1 = self.findBy(By.NAME, "modqg_01-0-original_modkey_01")
        self.assertIsNone(modkey_1.get_attribute("disabled"))

        # She goes to the sample questionnaire
        self.browser.get(sample_url)
        self.toggle_all_sections()

        # She edits some values
        self.click_edit_section("cat_1")
        self.findBy(By.NAME, "qg_1-0-original_key_1").send_keys(" (changed)")
        elm = self.findBy(By.ID, "id_qg_3-0-key_4")
        self.browser.execute_script("arguments[0].style.display='block';", elm)
        select = Select(elm)
        select.select_by_value("country_4")
        elm = self.findBy(By.ID, "button-submit")
        self.browser.execute_script("arguments[0].click();", elm)
        self.findBy(By.XPATH, '//div[contains(@class, "success")]')

        # She goes back to the module and sees the updated values
        self.browser.get(module_url)
        self.toggle_all_sections()
        self.findBy(By.XPATH, '//*[text()[contains(.,"Key 1 (Samplemodule)")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"Foo (changed)")]]')
        self.findByNot(By.XPATH, '//*[text()[contains(.,"Bar")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"Key 4 (Samplemodule)")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"Switzerland")]]')

        # She enters the first question and saves the step
        self.click_edit_section("modcat_1")
        self.findBy(By.NAME, "modqg_01-0-original_modkey_01").send_keys("asdf")
        self.submit_form_step()

        # She sees the inherited values and the first question
        self.findBy(By.XPATH, '//*[text()[contains(.,"Key 1 (Samplemodule)")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"Foo (changed)")]]')
        self.findByNot(By.XPATH, '//*[text()[contains(.,"Bar")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"Key 4 (Samplemodule)")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"Switzerland")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"ModKey 1")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"asdf")]]')

        # She goes back to the form and sees all values
        self.click_edit_section("modcat_1")
        key_1 = self.findBy(By.NAME, "modqg_sample_01-0-original_key_1")
        self.assertEqual(key_1.get_attribute("value"), "Foo (changed)")
        self.assertEqual(key_1.get_attribute("disabled"), "true")
        key_4 = self.findBy(By.NAME, "modqg_sample_02-0-key_4")
        self.assertEqual(key_4.get_attribute("disabled"), "true")
        key_4_selected = self.findBy(
            By.XPATH, '//select[@name="modqg_sample_02-0-key_4"]/option[@selected]'
        )
        self.assertEqual(key_4_selected.get_attribute("value"), "country_4")

        # By some hack, she makes the inherited value editable and submits the
        # step
        self.set_input_value(key_1, "spam")
        self.submit_form_step()

        # She sees the unchanged inherited values
        self.findBy(By.XPATH, '//*[text()[contains(.,"Key 1 (Samplemodule)")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"Foo (changed)")]]')
        self.findByNot(By.XPATH, '//*[text()[contains(.,"Bar")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"Key 4 (Samplemodule)")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"Switzerland")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"ModKey 1")]]')
        self.findBy(By.XPATH, '//*[text()[contains(.,"asdf")]]')
        self.findByNot(By.XPATH, '//*[text()[contains(.,"spam")]]')


@override_settings(IS_ACTIVE_FEATURE_MODULE=True)
class EditModuleWithLink(FunctionalTest):
    fixtures = [
        "groups_permissions",
        "global_key_values",
        "sample",
        "samplemodule",
        "samplemulti",
        "sample_samplemulti_questionnaires",
    ]

    def test_edit_module_with_link(self):
        """
        This tests a bugfix where there was an error when trying to add a module
        to a questionnaire which has a link.
        """
        # Alice logs in
        user = User.objects.get(pk=101)
        self.doLogin(user=user)

        # She goes to a SAMPLE questionnaire which has a link
        self.open_questionnaire_details("sample", identifier="sample_1")

        # She opens the panel to add a module and sees there is no error.
        self.wait_for(
            By.XPATH, '//a[contains(@class, "js-show-embedded-modules-form")]'
        )
        elm = self.findBy(
            By.XPATH, '//a[contains(@class, "js-show-embedded-modules-form")]'
        )
        self.browser.execute_script("arguments[0].click();", elm)
        self.findBy(By.XPATH, '//input[@value="samplemodule" and @name="module"]')

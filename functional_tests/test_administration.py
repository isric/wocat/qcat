from django.contrib.auth.models import Group
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

from apps.accounts.tests.test_models import create_new_user
from functional_tests.base import FunctionalTest


class AdminTest(FunctionalTest):
    fixtures = [
        "groups_permissions",
    ]

    def setUp(self):
        super().setUp()
        user = create_new_user()
        user.is_superuser = True
        user.save()
        self.user = user

    def test_admin_page_superuser(self):
        return
        # Alice logs in
        self.doLogin(user=self.user)

        # She sees the admin button in the top navigation bar and clicks on it
        self.clickUserMenu(self.user)
        navbar = self.findBy(By.CLASS_NAME, "top-bar")
        elm = navbar.find_element(By.LINK_TEXT, "Search Index Administration")
        self.browser.execute_script("arguments[0].click();", elm)

        column_1 = self.findBy(By.ID, "column_1")

        user_module = self.findBy(By.ID, "module_1", base=column_1)
        user_module.find_element(By.LINK_TEXT, "Users")
        configurations_module = self.findBy(By.ID, "module_3", base=column_1)
        configurations_module.find_element(By.LINK_TEXT, "Projects")
        with self.assertRaises(NoSuchElementException):
            column_1.find_element(By.ID, "module_6")
            column_1.find_element(By.ID, "module_7")

    def test_admin_page_translators(self):
        return
        user = create_new_user(id=2, email="foo@bar.com")
        user.groups.add(Group.objects.filter(name="Translators").first())
        user.is_superuser = True
        user.save()

        # Alice logs in
        self.doLogin(user=user)

        # She sees the admin button in the top navigation bar and clicks on it
        self.clickUserMenu(user)
        navbar = self.findBy(By.CLASS_NAME, "top-bar")
        elm = navbar.find_element(By.LINK_TEXT, "Administration")
        self.browser.execute_script("arguments[0].click();", elm)

        print(self.browser.page_source)
        column_1 = self.findBy(By.ID, "column_1")
        with self.assertRaises(NoSuchElementException):
            column_1.find_element(By.ID, "module_2")
            column_1.find_element(By.ID, "module_6")

    def test_admin_page_wocat_secretariat(self):
        return
        user = create_new_user(id=2, email="foo@bar.com")
        user.groups.add(Group.objects.filter(name="WOCAT Secretariat").first())
        user.save()

        # Alice logs in
        self.doLogin(user=user)

        # She sees the admin button in the top navigation bar and clicks on it
        self.clickUserMenu(user)
        navbar = self.findBy(By.CLASS_NAME, "top-bar")
        navbar.find_element(By.LINK_TEXT, "Administration").click()

        # She sees that she can edit projects in the admin section
        self.findBy(By.XPATH, '//h2[contains(text(), "Configuration")]')
        self.findBy(By.XPATH, '//strong[contains(text(), "Projects")]')

        # She clicks to add a new project and sees that she can edit the ID as
        # well
        self.findBy(
            By.XPATH, '//a[contains(@href, "/admin/configuration/project' '/add/")]'
        ).click()

        textfields = self.findManyBy(By.XPATH, '//input[@type="text"]')
        self.assertEqual(len(textfields), 2)

        # She goes back to the admin page
        self.browser.execute_script("window.history.go(-1)")

        # She can no longer edit institutions (they are managed in the CMS)
        self.findByNot(By.XPATH, '//strong[contains(text(), "Institutions")]')

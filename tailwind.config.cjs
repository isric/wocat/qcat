/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./apps/summary/templates/summary/**/*.html"],
  theme: {
    extend: {
      colors: {
        green1: "#557431",
        orange1: "#D26929",
      },
      screens: {
        print: { raw: "print" },
      },
      opacity: { 15: ".15" },
    },
  },
  plugins: [],
};

import puppeteer from "puppeteer";
import { writeFile } from "fs";

(async () => {
  const [_node, _script, file_path, url, footer1, footer2] = process.argv;

  const browser = await puppeteer.launch({ args: ["--no-sandbox"] });
  const page = await browser.newPage();

  // await page.setViewport({ width: 794, height: 1122, deviceScaleFactor: 1 });
  const response = await page.goto(url, { waitUntil: "networkidle0" });
  if (response.status() !== 200) {
    await browser.close();
    return;
  }

  await page.emulateMediaType("screen");

  const pdf = await page.pdf({
    format: "A4",
    printBackground: true,
    displayHeaderFooter: true,
    headerTemplate: `<div></div>`,
    footerTemplate: `
<div style="margin: 0 9mm 0 9mm; width: 100%; font-size: 9px; display: flex; justify-content: space-between; color: #eeeeee;">
<div style="">${footer2}</div>
<div style="text-align: center">${footer1}</div>
<div style=""><span class="pageNumber"></span>/<span class="totalPages"></span></div>
</div>
  `,
  });

  writeFile(file_path, pdf, { flag: "w+" }, (err) => {});
  // // return pdf;
  // console.log(pdf.toString("base64"));
  await browser.close();
})();
